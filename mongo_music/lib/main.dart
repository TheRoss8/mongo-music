import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'dart:async' show Future;
import 'package:flutter_slidable/flutter_slidable.dart';

Future<MusicCollection> fetchAlbums() async {
  final response =
  await http.get('https://webhooks.mongodb-stitch.com/api/client/v2.0/app/music-rjvku/service/get/incoming_webhook/getLP');
  print("CIAOOOO");
  if (response.statusCode == 200) {
    final std = json.decode(response.body);
    MusicCollection address = new MusicCollection.fromJson(std);
    return address;
  } else {
    throw Exception('Failed to load post');
  }
}


Future<void> deleteOne(String title) async {
  String command =
      'https://webhooks.mongodb-stitch.com/api/client/v2.0/app/music-rjvku/service/delete/incoming_webhook/deleteLP?title='
          + title;
  final response =
  await http.delete(command);
  print("CIAOOOO");
  if (response.statusCode == 200) {
    runApp(MyApp(music: fetchAlbums()));
  } else {
    throw Exception('Failed to load post');
  }
}


class MusicCollection{
  final List<Album> albums;
  MusicCollection({this.albums});

  factory MusicCollection.fromJson(List<dynamic> parsedJson){
    List<Album> albumList = new List<Album>();
    albumList = parsedJson.map((i) => Album.fromJson(i)).toList();

    return new MusicCollection(
      albums: albumList,
    );
  }
}


class Album{

  final String id;
  final String title;
  final String year;
  final String type;
  final String img;
  final List<Track> tracks;
  final List<Artist> artists;

  Album({
    this.id,
    this.title,
    this.year,
    this.type,
    this.img,
    this.tracks,
    this.artists
  });

  factory Album.fromJson(Map<String, dynamic> albumJson){

    var songs = albumJson['tracks'] as List;
    var composers = albumJson['artists'] as List;
    List<Track> trackList = songs.map((i) => Track.fromJson(i)).toList();
    List<Artist> artistsList = composers.map((i) => Artist.fromJson(i)).toList();



    return new Album(
        id: albumJson['_id'].toString(),
        title: albumJson['title'].toString(),
        year: albumJson['year'].toString(),
        type: albumJson['type'].toString(),
        img: albumJson['img'].toString(),
        tracks: trackList,
        artists: artistsList
    );
  }
}


class Track{
  final String title;
  final String length;

  Track({
    this.title,
    this.length
  });

  factory Track.fromJson(Map<String, dynamic> json) {
    return new Track(
        title: json['title'],
        length: json['length']
    );
  }
}

class Artist{
  final String name;

  Artist({
    this.name
  });


  factory Artist.fromJson(Map<String, dynamic> json){
    return new Artist(
      name: json['name']
    );
  }
}



void main() => runApp(MyApp(music: fetchAlbums()));

class MyApp extends StatelessWidget {
  final Future<MusicCollection> music;

  MyApp({Key key, this.music}) : super(key: key);



  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MongoMusic',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('MongoMusic'),
        ),
        body: Center(
            child: new RefreshIndicator(
              child: populate(),
              onRefresh: () => deleteOne(""),
            )
        ),
      ),
    );
  }

  FutureBuilder<MusicCollection> populate(){
    return new FutureBuilder<MusicCollection>(
      future: music,
      builder: (context, snapshot) {
        if(snapshot.hasData){
          List<Album> collection = snapshot.data.albums;
          return new ListView.builder(
            itemCount: collection.length,
            itemBuilder: (BuildContext context, int index) {
              List<Track> tracks = collection[index].tracks;
              List<Widget> list = new List<Widget>();
              for(int i = 0; i < tracks.length; i++){
                List<Widget> line = new List<Widget>();

                line.add(new Expanded(
                    child: new Container(
                        padding: new EdgeInsets.only(left: 20.0, top: 10.0, bottom: 10.0, right: 20.0),
                        child: new Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[

                              new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  new Text(tracks[i].title,
                                    style: new TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                ],
                              ),
                              new Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  new Text(tracks[i].length,
                                    style: new TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w200,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                ],
                              ),
                            ]

                        )

                    )
                )
                );
                list.add(
                    new Row(
                      children: line,
                    )
                );
              }

              return new Slidable(
                delegate: new SlidableStrechDelegate(),
                actionExtentRatio: 0.25,

                child:
                ExpansionTile(
                    title:new Row(

                      children: <Widget>[
                        Image.network(collection[index].img, width: 50.0, height: 50.0),
                        Container(width: 10.0, height: 50.0),
                        Column(

                          crossAxisAlignment: CrossAxisAlignment.start,

                          children: <Widget>[
                            Text(collection[index].title),
                            Text(collection[index].artists[0].name, style: new TextStyle(
                              fontWeight: FontWeight.w200,

                            ),)
                          ],
                        )
                      ],
                    ),

                    children: <Widget>[
                      new Column(
                        children: list,
                      ),
                    ]
                ),



                secondaryActions: <Widget>[
                  IconSlideAction(
                    caption: 'Delete',
                    color: Colors.red,
                    icon: Icons.delete,
                    onTap: () {
                      String title = collection[index].title;
                      deleteOne(title);
                      Scaffold
                          .of(context)
                          .showSnackBar(SnackBar(content: Text("Removed " + title)));
                    },
                  ),
                ],



              );




//              return new Dismissible(
//                  background: Container(color: Colors.red),
//                  key: Key(collection[index].title),
//                  onDismissed: (direction) {
//                    String title = collection[index].title;
//                    deleteOne(title);
//                    Scaffold
//                        .of(context)
//                        .showSnackBar(SnackBar(content: Text("Removed " + title)));
//                  },
//                  child:
//
//                  ExpansionTile(
//                      title: new Text(collection[index].title),
//                      children: <Widget>[new Column(
//                        children: list,
//                      ),
//                      ]
//                  )
//              );






            },
          );

        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }

        // By default, show a loading spinner
        return CircularProgressIndicator();
      },
    );
  }


}

