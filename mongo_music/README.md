# Mongo Music
###### Never gonna give you up, never gonna let you down...


Mongo Music permette l'interazione con l'omonimo database ad oggetti contenente una raccolta di album musicali.

L'interfaccia, sviluppata utilizzando il framework Flutter e seguendo i dettami del Material Design, permette la visualizzazione degli album nella raccolta con anche gli artisti coinvolti.
Toccando il singolo album, questo si espande fornendo informazioni circa le tracce contenute e la loro lunghezza.
Per ogni album è presente anche l'immagine di copertina.
È possibile inoltre eliminare un album facendo uno swipe da destra verso sinistra e confermarne l'eliminazione toccando il pulsante elmina. L'effettiva eliminazione dal database verrà confermata da una `SnackBar` nella parte inferiore dello schermo.

---

## Esempio deserializzazione

Una volta prelevati i dati dal database sotto forma di stringa `JSON`, è necessario deserializzarli, ovvero "scomporli" in oggetti manipolabili dall'applicazione. 
Si fa uso dimetodi factory come quello mostrato di seguito per la manipolazione degli oggetti `Album`.
Si noti l'utilizzo di mappe e liste per i campi `tracks` e `artists`
```js

Album({
    this.id,
    this.title,
    this.year,
    this.type,
    this.img,
    this.tracks,
    this.artists
  });

  factory Album.fromJson(Map<String, dynamic> albumJson){
    var songs = albumJson['tracks'] as List;
    var composers = albumJson['artists'] as List;
    List<Track> trackList = songs.map((i) => Track.fromJson(i)).toList();
    List<Artist> artistsList = composers.map((i) => Artist.fromJson(i)).toList();

    return new Album(
        id: albumJson['_id'].toString(),
        title: albumJson['title'].toString(),
        year: albumJson['year'].toString(),
        type: albumJson['type'].toString(),
        img: albumJson['img'].toString(),
        tracks: trackList,
        artists: artistsList
    );
  }
}
```

---

#Screenshots

| Pull to refresh | Tracklist | Delete button | Snackbar |
| --------------- | --------- | ------------- | -------- |
| ![Pull to refresh](https://francescorossato.ddns.net/mongo/1.png) | ![List](https://francescorossato.ddns.net/mongo/2.png) | ![Delete button](https://francescorossato.ddns.net/mongo/3.png) | ![Delete snackbar](https://francescorossato.ddns.net/mongo/4.png) |



>I, I wish you could swim,
>like the dolphins,
>like dolphins can swim

_Heroes, David Bowie_



